#include "Camera.h"

Camera::Camera(glm::vec3 pos, glm::vec3 front, glm::vec3 up)
    : position(pos), front(front), up(up), yaw(-90), pitch(0)
{

}

glm::mat4 Camera::GetViewMatrix() const
{
    glm::mat4 view = glm::lookAt(position, position + front, up);
    return view;
}

void Camera::Move(glm::vec3 d)
{
    position += d;
}

void Camera::Update(float dt, KeyInput& kbd)
{
    float cam_speed = static_cast<float>(2.5 * dt);

    glm::vec3 cam_move = glm::vec3(0);

    if(kbd.getIsKeyDown(GLFW_KEY_W))
        cam_move += cam_speed * front;
    if(kbd.getIsKeyDown(GLFW_KEY_S))
        cam_move -= cam_speed * front;
    if(kbd.getIsKeyDown(GLFW_KEY_A))
        cam_move -= glm::normalize(glm::cross(front, up)) * cam_speed;
    if(kbd.getIsKeyDown(GLFW_KEY_D))
        cam_move += glm::normalize(glm::cross(front, up)) * cam_speed;
    if(kbd.getIsKeyDown(GLFW_KEY_Z))
        cam_move -= cam_speed * up;
    if(kbd.getIsKeyDown(GLFW_KEY_C))
        cam_move += cam_speed * up;

    Move(cam_move);

    float rotate_speed = static_cast<float>(50 * dt);
    if(kbd.getIsKeyDown(GLFW_KEY_UP))
        pitch += rotate_speed;
    if(kbd.getIsKeyDown(GLFW_KEY_DOWN))
        pitch -= rotate_speed;
    if(kbd.getIsKeyDown(GLFW_KEY_LEFT))
        yaw -= rotate_speed;
    if(kbd.getIsKeyDown(GLFW_KEY_RIGHT))
        yaw += rotate_speed;

    if(pitch > 89.0f)
        pitch = 89.0f;
    if(pitch < -89.0f)
        pitch = -89.0f;

    glm::vec3 direction;
    direction.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
    direction.y = sin(glm::radians(pitch));
    direction.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));

    front = glm::normalize(direction);
}
