#ifndef CAMERA_H_SENTRY
#define CAMERA_H_SENTRY

#include "KeyInput.h"
#include "glm/glm.hpp"
#include <glm/ext/matrix_transform.hpp>

class Camera {
public:
    Camera(
        glm::vec3 pos = glm::vec3(0.0f),
        glm::vec3 front = glm::vec3(0.0f,0.0f,-1.0f),
        glm::vec3 up = glm::vec3(0.0f,1.0f,0.0f)
    );
    glm::mat4 GetViewMatrix() const;
    void Move(glm::vec3 d);
    void Update(float dt, KeyInput& kbd);
private:
    glm::vec3 position;
    glm::vec3 front;
    glm::vec3 up;
    float yaw;
    float pitch;
};

#endif
