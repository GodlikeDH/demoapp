#include <iostream>
#include <glm/glm.hpp>

#include "DemoApp.h"
#include "ResourceManager.h"

#include "Sphere.h"
#include "Square.h"
#include "Cube.h"
#include "Tetrahedron.h"

static void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    glViewport(0, 0, width, height);
}

DemoApp::DemoApp(unsigned int width, unsigned int height)
    : width(width), height(height), sc{},
    kbd {}
{
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_RESIZABLE, false);
    window = glfwCreateWindow(
        width,
        height,
        "DemoApp",
        nullptr,
        nullptr
    );
    glfwMakeContextCurrent(window);
    KeyInput::setupKeyInputs(window);
    // glad: load all OpenGL function pointers
    // ---------------------------------------
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        std::exit(-1);
    }

//    glfwSetKeyCallback(window, key_callback);
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

    // OpenGL configuration
    // --------------------
    glViewport(0, 0, width, height);

    // initialize demo
    // ---------------
    Init();
}

DemoApp::~DemoApp()
{
    sc.Clear();
    ResourceManager::Clear();
    glfwTerminate();
}

void DemoApp::Init()
{
    ResourceManager::LoadShader(
        "shaders/vertex.vs",
        "shaders/fragment.fs",
        nullptr,
        "default"
    );

    /* demo creates shapes => it should free it */
    Object sphere = {
        new Sphere(),
        {0,0,0},
        glm::vec3(1.0f),
    };
    sc.AddObject(sphere);

    Object cube = {
        new Cube(),
        {2,2,2},
        glm::vec3(1.0f),
        glm::vec3(1.0f, 0.0f, 0.0f),
        45.0f
    };
    sc.AddObject(cube);

    Object tet = {
        new Tetrahedron(),
        {-3, -3, -3},
        glm::vec3(2.0f)
    };
    sc.AddObject(tet);

    tet.SetShapeVertexColor(0, {1,0,0});
    tet.SetShapeVertexColor(1, {0,1,0});
    tet.SetShapeVertexColor(2, {0,0,1});
    tet.SetShapeVertexColor(3, {0,0,0});

    sc.MoveCamera({0,0,10});
}

void DemoApp::Update(float dt, KeyInput& kbd)
{
    if(kbd.getIsKeyDown(GLFW_KEY_ESCAPE)) {
        glfwSetWindowShouldClose(window, GLFW_TRUE);
    }
    sc.Update(dt, kbd);
}

void DemoApp::Render()
{
    Shader def = ResourceManager::GetShader("default");

    glm::mat4 projection = glm::perspective(
        glm::radians(45.0f),
        static_cast<float>(width) / height,
        0.1f,
        100.0f
    );
    def.SetMatrix4("projection", projection);

    sc.Draw(def);
}

void DemoApp::Go()
{

    // deltaTime variables
    // -------------------
    float deltaTime = 0.0f;
    float lastFrame = 0.0f;

    while (!glfwWindowShouldClose(window))
    {

        // calculate delta time
        // --------------------
        float currentFrame = glfwGetTime();
        deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;
        glfwPollEvents();

        // update state
        // -----------------
        Update(deltaTime, kbd);

        // render
        // ------
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);
        Render();

        glfwSwapBuffers(window);
    }
}
