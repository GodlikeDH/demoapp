#ifndef DEMOAPP_H_SENTRY
#define DEMOAPP_H_SENTRY
#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include "KeyInput.h"
#include "Scene.h"

class DemoApp {
public:
    unsigned int width, height;
    bool keys_pressed[1024];
    DemoApp(unsigned int width, unsigned int height);
    ~DemoApp();
    void Init();
    void Update(float dt, KeyInput& kbd);
    void Render();
    void Go();
private:
    Scene sc;
    GLFWwindow* window;
    KeyInput kbd;
};

#endif
