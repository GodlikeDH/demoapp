#include "Object.h"
#include <glm/ext/matrix_transform.hpp>
#include <glm/gtx/quaternion.hpp>

void Object::Update(float dt, KeyInput& kbd)
{
    position += speed * dt;
    rotAngle += rot_speed * dt;
    scale = glm::sin(scale_speed * dt) + scale;
}

void Object::Draw(Shader &shader)
{
    glm::mat4 model = glm::mat4(1.0f);

    model = glm::translate(model, position);
    model = glm::rotate(model, rotAngle, rotAxis);
    model = glm::scale(model, scale);

    shader.SetMatrix4("model", model);
    shape->Draw(shader);
}

void Object::Clear()
{
    delete shape;
}

void Object::SetShapeVertexColor(std::size_t index, glm::vec3 color)
{
    shape->vertex_data[index].color = color;
    shape->RefreshColors();
}

