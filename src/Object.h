#ifndef OBJECT_H_SENTRY
#define OBJECT_H_SENTRY

#include "KeyInput.h"
#include "Shape.h"
#include <memory>


class Object {
public:
    Object(Shape* shape, glm::vec3 pos,
           glm::vec3 scale = glm::vec3(1.0f),
           glm::vec3 rotAxis = glm::vec3(1.0f, 0.0f, 0.0f),
           float rotAngle = 0
    ) : shape(shape), position(pos), scale(scale),
        rotAxis(rotAxis), rotAngle(rotAngle),
        speed(0), scale_speed(0), rot_speed(0)
    {
    };
    void Draw(Shader &shader);
    void Update(float dt, KeyInput& kbd);
    void Clear();
    void SetShapeVertexColor(std::size_t index, glm::vec3 color);
private:
    Shape* shape;
    glm::vec3 position;
    glm::vec3 scale;
    glm::vec3 rotAxis;
    float rotAngle;

    glm::vec3 speed;
    float scale_speed;
    float rot_speed;
};

#endif
