#ifndef RESOURCEMANAGER_H_SENTRY
#define RESOURCEMANAGER_H_SENTRY

#include <map>
#include <string>

#include <glad/glad.h>

#include "Shader.h"
#include "Shape.h"

class ResourceManager
{
public:
    // resource storage
    static std::map<std::string, Shader> Shaders;

    // loads (and generates) a shader program from file loading vertex,
    // fragment (and geometry) shader's source code.
    static Shader LoadShader(
        const char *vShaderFile,
        const char *fShaderFile,
        const char *gShaderFile,
        std::string name
    );

    // retrieves a stored shader
    static Shader GetShader(std::string name);
    // properly de-allocates all loaded resources
    static void Clear();
private:
    ResourceManager() {}

    // loads and generates a shader from file
    static Shader loadShaderFromFile(
        const char *vShaderFile,
        const char *fShaderFile,
        const char *gShaderFile = nullptr
    );
};

#endif
