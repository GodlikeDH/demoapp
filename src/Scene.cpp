#include "Scene.h"

Scene::Scene(std::vector<Object>& objects)
    : cam({0,0,0}), objects(std::move(objects))
{
}

void Scene::Update(float dt, KeyInput& kbd)
{
    cam.Update(dt, kbd);
    for(auto& o : objects) {
        o.Update(dt, kbd);
    }
}

void Scene::Draw(Shader &shader)
{

    glm::mat4 view = cam.GetViewMatrix();

    shader.SetMatrix4("view", view);
    for(auto& o : objects) {
        o.Draw(shader);
    }

}

void Scene::AddObject(const Object& obj)
{
    objects.push_back(obj);
}

void Scene::Clear()
{
    for(auto& o : objects) {
        o.Clear();
    }
}

void Scene::MoveCamera(glm::vec3 d)
{
    cam.Move(d);
}
