#ifndef SCENE_H_SENTRY
#define SCENE_H_SENTRY

#include "Object.h"
#include "Camera.h"
#include "KeyInput.h"

#include <memory>

class Scene {
public:
    Scene() {};
    Scene(std::vector<Object>& objects);
    void AddObject(const Object& obj);
    void Draw(Shader &shader);
    void Update(float dt, KeyInput& kbd);
    void Clear();
    void MoveCamera(glm::vec3 d);
private:
    Camera cam;
    std::vector<Object> objects;
};
#endif

