#include "Shape.h"

#include <glad/glad.h>
#include <iostream>

void Shape::RefreshColors()
{
    glBindVertexArray(VAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(
        GL_ARRAY_BUFFER,
        vertex_data.size() * sizeof(Vertex),
        vertex_data.data(),
        GL_STATIC_DRAW
    );
    // vertex colors
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(
        1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex),
        (void*)offsetof(Vertex, color)
    );

    glBindVertexArray(0);
}

Shape::Shape(std::vector<Vertex> vd, std::vector<GLuint> id)
    : vertex_data(std::move(vd)), index_data(std::move(id))
{
    // create buffers/arrays
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    glGenBuffers(1, &EBO);

    glBindVertexArray(VAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(
        GL_ARRAY_BUFFER,
        vertex_data.size() * sizeof(Vertex),
        vertex_data.data(),
        GL_STATIC_DRAW
    );

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(
        GL_ELEMENT_ARRAY_BUFFER,
        index_data.size() * sizeof(unsigned int),
        index_data.data(),
        GL_STATIC_DRAW
    );

    // vertex Positions
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(
        0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0
    );

    // vertex colors
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(
        1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex),
        (void*)offsetof(Vertex, color)
    );

    glBindVertexArray(0);
}

void Shape::Draw(Shader &shader)
{
    shader.Use();
    glBindVertexArray(VAO);
    glDrawElements(
        GL_TRIANGLES,
        static_cast<unsigned int>(index_data.size()),
        GL_UNSIGNED_INT,
        0
    );
    glBindVertexArray(0);
}
