#ifndef SHAPE_H_SENTRY
#define SHAPE_H_SENTRY

#include "Vertex.h"
#include "Shader.h"
#include <glad/glad.h>
#include <vector>

class Shape {
public:
    std::vector<Vertex> vertex_data;
    std::vector<GLuint> index_data;
    void Draw(Shader &shader);
    Shape(
        std::vector<Vertex> vd,
        std::vector<GLuint> id
    );
    void RefreshColors();
private:
    unsigned int VBO, EBO, VAO;
};

#endif
