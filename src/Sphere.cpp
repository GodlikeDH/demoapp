#include "Sphere.h"
#include <glm/gtx/quaternion.hpp>
#include <utility>
#include <glm/ext/quaternion_trigonometric.hpp>

static std::pair<std::vector<Vertex>, std::vector<GLuint>>
    generateSphereVertexes(int latDiv = 24, int longDiv = 48)
{
    std::vector<Vertex> vd;
    std::vector<GLuint> id;

    const float lattitudeAngle = glm::pi<float>() / latDiv;
    const float longitudeAngle = 2.0f * glm::pi<float>() / longDiv;

    /* sphere coords -> cartesian */
    for(int iLat = 1; iLat < latDiv; iLat++) {

        float z = glm::cos(lattitudeAngle * iLat);

        for(int iLong = 0; iLong < longDiv; iLong++)
        {
           float x = glm::sin(lattitudeAngle * iLat) * glm::cos(longitudeAngle * iLong);
           float y = glm::sin(lattitudeAngle * iLat) * glm::sin(longitudeAngle * iLong);

           vd.push_back({x, y, z});
        }
    }

    GLuint iNorthPole = vd.size();
    vd.push_back({0,0,1});

    GLuint iSouthPole = vd.size();
    vd.push_back({0,0,-1});

    const auto calcIdx = [longDiv]( int iLat,int iLong )
                        { return iLat * longDiv + iLong; };

    for(int iLat = 0; iLat < latDiv - 2; iLat++) {
        for(int iLong = 0; iLong < longDiv - 1; iLong++) {
            id.push_back( calcIdx( iLat,iLong ) );
            id.push_back( calcIdx( iLat + 1,iLong ) );
            id.push_back( calcIdx( iLat,iLong + 1 ) );
            id.push_back( calcIdx( iLat,iLong + 1 ) );
            id.push_back( calcIdx( iLat + 1,iLong ) );
            id.push_back( calcIdx( iLat + 1,iLong + 1 ) );
        }

        id.push_back( calcIdx( iLat,longDiv - 1 ) );
        id.push_back( calcIdx( iLat + 1,longDiv - 1 ) );
        id.push_back( calcIdx( iLat,0 ) );
        id.push_back( calcIdx( iLat,0 ) );
        id.push_back( calcIdx( iLat + 1,longDiv - 1 ) );
        id.push_back( calcIdx( iLat + 1,0 ) );
    }

    // cap fans
    for( int iLong = 0; iLong < longDiv - 1; iLong++ ) {
            // north
            id.push_back( iNorthPole );
            id.push_back( calcIdx( 0,iLong ) );
            id.push_back( calcIdx( 0,iLong + 1 ) );
            // south
            id.push_back( calcIdx( latDiv - 2,iLong + 1 ) );
            id.push_back( calcIdx( latDiv - 2,iLong ) );
            id.push_back( iSouthPole );
    }
    // wrap triangles
    // north
    id.push_back( iNorthPole );
    id.push_back( calcIdx( 0,longDiv - 1 ) );
    id.push_back( calcIdx( 0,0 ) );
    // south
    id.push_back( calcIdx( latDiv - 2,0 ) );
    id.push_back( calcIdx( latDiv - 2,longDiv - 1 ) );
    id.push_back( iSouthPole );

    return {std::move(vd), std::move(id)};
}

static const auto data = generateSphereVertexes();

Sphere::Sphere()
    : Shape(data.first, data.second)
{

};
