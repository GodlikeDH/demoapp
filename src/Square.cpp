#include "Square.h"

static const std::vector<Vertex> v {
   { 0.5f,  0.5f, 0.0f},  // top right
   { 0.5f, -0.5f, 0.0f},  // bottom right
   {-0.5f, -0.5f, 0.0f},  // bottom left
   {-0.5f,  0.5f, 0.0f},  // top left
};

static const std::vector<GLuint> i {
    0, 1, 3,
    1, 2, 3,
};

Square::Square() : Shape(v, i) {};
