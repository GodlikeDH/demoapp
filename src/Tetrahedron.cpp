#include "Tetrahedron.h"

static const std::vector<Vertex> vertices = {
    {  1,  1,  1 },
    {  1, -1, -1 },
    { -1,  1, -1 },
    { -1, -1,  1 }
};

static const std::vector<GLuint> indexes = {
    0, 1, 2,
    0, 1, 3,
    0, 2, 3
};

Tetrahedron::Tetrahedron() : Shape(vertices, indexes) {};
