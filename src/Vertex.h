#ifndef VERTEX_H_SENTRY
#define VERTEX_H_SENTRY

#include <glm/glm.hpp>

struct Vertex {
    Vertex(float x, float y, float z)
        : pos(x,y,z), color(glm::vec3(0.5f)) {};
    Vertex(glm::vec3 pos, glm::vec3 color = glm::vec3(0.5f))
        : pos(pos), color(color) {};
    glm::vec3 pos;
    glm::vec3 color;
};

#endif
