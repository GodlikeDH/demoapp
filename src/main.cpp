#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include "DemoApp.h"
#include "ResourceManager.h"

#include <iostream>

// The Width of the screen
constexpr unsigned int SCREEN_WIDTH = 800;
// The height of the screen
constexpr unsigned int SCREEN_HEIGHT = 600;


int main()
{
    DemoApp app(SCREEN_WIDTH, SCREEN_HEIGHT);
    app.Go();
    return 0;
}


